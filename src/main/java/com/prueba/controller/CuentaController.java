package com.prueba.controller;

import com.prueba.entity.Cuenta;
import com.prueba.exception.BusinessException;
import com.prueba.model.ResponseData;
import com.prueba.model.RespuestaError;
import com.prueba.service.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/prueba")
@CrossOrigin(origins = "*")
public class CuentaController {

    @Autowired
    CuentaService cuentaService;

    @GetMapping("/cuentas")
    public ResponseEntity<?> getCuentas(){

        ResponseData<Object> response = new ResponseData<Object>();

        try{
            List<Cuenta> listCuentas = cuentaService.getAllCluentas();
            if (listCuentas != null && !listCuentas.isEmpty()) {
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setData(listCuentas);
                response.setMessage("Registros encontrados.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else {
                response.setSuccess(true);
                response.setData(new ArrayList<>());
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("Registros vacios.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }



    }

    @GetMapping("/cuentas/{id}")
    public ResponseEntity<?> getCuentaById(@PathVariable("id") Long idCuenta){

        ResponseData<Object> response = new ResponseData<Object>();

        try {
            Cuenta cuenta = cuentaService.getByIdCuenta(idCuenta);

            if (cuenta != null ) {
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setData(cuenta);
                response.setMessage("Cliente encontrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else {
                response.setSuccess(true);
                response.setData(new ArrayList<>());
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("Cliente no encontrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }


    }

    @PostMapping ("/cuentas")
    public ResponseEntity<?> postCuenta(@RequestBody Cuenta request){
        ResponseData<Object> response = new ResponseData<Object>();

        try{

            Cuenta cuentaResponse = cuentaService.saveCuenta(request);

            if(cuentaResponse != null){
                response.setNotificaciones(new ArrayList<>());
                response.setData(cuentaResponse);
                response.setMessage("Cuenta registrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else{
                response.setNotificaciones(new ArrayList<>());
                response.setData(request);
                response.setMessage("Cuenta no registrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }

        }catch (BusinessException be ){
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }


    }

    @PutMapping ("/cuentas")
    public ResponseEntity<?> putCuenta(@RequestBody Cuenta request){

        ResponseData<Object> response = new ResponseData<Object>();

        try{
            if(request != null && request.getIdCuenta()!= null){
                Cuenta cuentaResponse = cuentaService.updateCuenta(request);

                if(cuentaResponse != null){
                    response.setSuccess(true);
                    response.setNotificaciones(new ArrayList<>());
                    response.setData(cuentaResponse);
                    response.setMessage("Cuenta actualizado.");
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }else{
                    response.setSuccess(true);
                    response.setNotificaciones(new ArrayList<>());
                    response.setData(request);
                    response.setMessage("Cuenta no actualizado.");
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }

            }else{
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("El id de la cuenta no presente.");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (BusinessException be ){
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }

    }


    @DeleteMapping("/cuentas/{id}")
    public ResponseEntity<?> deleteCuentaById(@PathVariable("id") Long idCuenta){
        ResponseData<Object> response = new ResponseData<Object>();

        try {
            cuentaService.deleteCuenta(idCuenta);
            response.setSuccess(true);
            response.setData(new ArrayList<>());
            response.setNotificaciones(new ArrayList<>());
            response.setMessage("Cuenta eliminado.");
            return new ResponseEntity<>(response, HttpStatus.OK);

        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }
    }

}
