package com.prueba.controller;

import com.prueba.entity.Movimientos;
import com.prueba.exception.BusinessException;
import com.prueba.model.Reporte;
import com.prueba.model.ResponseData;
import com.prueba.model.RespuestaError;
import com.prueba.service.MovimientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/prueba")
@CrossOrigin(origins = "*")
public class ReporteController {

    @Autowired
    MovimientoService movimientoService;


    @GetMapping("/reportes")
    public ResponseEntity<?> getCuentas(
            @RequestParam(value = "fechaInicio", required = true)String fechaInicio,
            @RequestParam(value = "fechaFin", required = true)String fechaFin,
            @RequestParam(value = "numeroCuenta", required = true)String numeroCuenta
    ) {

        ResponseData<Object> response = new ResponseData<Object>();

        try{
            List<Reporte> listReporte = movimientoService.getReporte(fechaInicio,fechaFin,numeroCuenta);
            if (listReporte != null && !listReporte.isEmpty()) {
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setData(listReporte);
                response.setMessage("Registros encontrados.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else {
                response.setSuccess(true);
                response.setData(new ArrayList<>());
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("Registros vacios.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }

    }

}
