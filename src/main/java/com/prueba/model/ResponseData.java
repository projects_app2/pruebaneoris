package com.prueba.model;

import lombok.Builder;
import lombok.Data;

@Data
public class ResponseData<T> {

    private Boolean success;
    private String message;
    private T notificaciones;
    private T data;


}
